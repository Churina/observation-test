import React from 'react'

export default function Card({value,  currentNumber, isClicked}){
     const styles = {
         backgroundColor: value < currentNumber ? "#59E391" : "white"
    }
    
    return(
        <div 
        className='card-face'
        style={styles} 
        onClick={() => isClicked(value)}
        >
            <h2 className='card-num'>{value}</h2>
        </div>
    )
}

