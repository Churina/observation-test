import React from 'react';
import Card from'./components/Card'
import './App.css';

function App() {

  const [currentNumber, setCurrentNumber] = React.useState(1)
  const [click, setClick] = React.useState({evenNumberClick:0, oddNumberClick:0})
  const [numbers, setNumbers] = React.useState(numberArray ())
  const [startTime, setStartTime] = React.useState(new Date())

  
  function numberArray () {
    const evenNumbers = [];
    const oddNumbers = [];

    for(let i = 1; i <= 25; i++){
      if (i % 2 === 0) {
        evenNumbers.push(i);
      } else {
        oddNumbers.push(i);
      }
    }
    return { 
      evenNumbers: shuffle(evenNumbers),
      oddNumbers: shuffle(oddNumbers)
    }
  } 


  function shuffle(numbers) {
    const newNumbers = [...numbers]
    for (let i = newNumbers.length - 1 ; i > 0 ; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [newNumbers[i], newNumbers[j]] = [newNumbers[j], newNumbers[i]];
    }
    return newNumbers;
    }


  const createCards = (numbers) => numbers.map(number => {
    return (
      <Card 
      key={number} 
      value={number} 
      isClicked={clickHandler}
      currentNumber={currentNumber}
      />
    )
  })


  function clickHandler(number) {
     const click = number % 2 === 0 ? "evenNumberClick" : "oddNumberClick"
     const numbers = number % 2=== 0 ? "evenNumbers" :"oddNumbers" 

    if(currentNumber === number) {

      setCurrentNumber(currentNumber + 1)

      setClick(preClick => ({
        ...preClick,
        [click] : preClick[click] + 1
      }))
  

    } else {
        setNumbers(preNumbers => ({
          ...preNumbers,
          [numbers]:shuffle(preNumbers[numbers])
        }))

        setClick(preClick => ({
          ...preClick,
          [click]:preClick[click] + 1
        }))
    }
  }


 function startGame() {
    setCurrentNumber(1)
    setClick({evenNumberClick:0, oddNumberClick:0})
    setNumbers(numberArray)
    setStartTime = new Date()
  } 

 
  function winMessage(){
    const score = Math.round(25/(click.evenNumberClick + click.oddNumberClick) * 100)
 
     if (currentNumber > 25) {
       return (
         <div>
            <h2 className='win'>YOU WIN!</h2>
            <h2>Your score: {score} </h2>
            <h2>Total time: {totalTime()} seconds</h2>
         </div>
       )
     }
   }

  function totalTime(){

    const currentTime = new Date()
    const timeInMilliseconds = currentTime.valueOf() - startTime.valueOf()
    return Math.round(timeInMilliseconds / 1000)
  }


  return (
    <main>
      <div className='title-container'>
        <h1>Observation Test</h1>
        <button onClick={startGame} >Start Game</button>
      </div>
      
      <div className='card-container'>
        <div className='sub-container'>
          <h2>Odd Numbers Clicks:{click.oddNumberClick}</h2>
          <div className='odd-container'>{createCards(numbers.oddNumbers)}</div>
        </div>
        <div className='sub-container'>
          <h2>Even Numbers Clicks:{click.evenNumberClick}</h2>
          <div className='even-container'>{createCards(numbers.evenNumbers)}</div>
        </div>
      </div>  

      <div className='win-message'>
        <div>{winMessage()}</div>
        <h3 className='hint'>HINT: next number {currentNumber}</h3>
      </div>  
    </main>  
    )
}

export default App;
