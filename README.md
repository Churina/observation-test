                                           **Front end observation test exercise v1**

The nature tour company, Animal Spotters, is hiring tour guides again. They want applicants to take an observational test that will eventually be used to pre-rank applicants prior to any interviews. The previous proof of concept (POC) you built for them was a great success and they want to iterate on it by making it slightly more challenging and using a modern web framework. Applicants will see a screen divided into two sections filled with numbers. Their job is to click the numbers in order from 1 to the max.

- Requirements:

1. The page should display two separate sections with a grid of boxes containing the numbers 1-25: the left one showing odd numbers and the right one showing even numbers.
2. Numbers should be displayed in a randomized order in each section.
3. When an applicant clicks a number, if it is the next number in order, then its container background turns a different color for the remainder of the test. If it is the wrong number, the section should have its numbers shuffled (ex. A wrong guess in the odd section should cause the numbers in the odd section to be shuffled).
4. Each grid should display the total amount of number clicks for its section.
5. Once all the numbers have been clicked in order, a win message should be displayed with the applicant's test score: the percentage of correct number clicks and the applicant’s total time taken.
6. Include a button to start a new game at any time.
7. Application should be broken down into multiple, logical components, stored in separate files.
8. Component state should be updated immutably. 

- Stretch Requirements (These are optional requirements for additional practice):

1. Create a modal component that shows the success message when a user finishes.
2. Numbers will now be in the Fibonacci sequence, starting with 1,2, and must be clicked in that order. (1,2,3,5,8,13…).
